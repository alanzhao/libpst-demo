package com.example;

import com.pff.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Vector;

@SpringBootApplication
public class LibpstDemoApplication {

	public static void main(String[] args) {

        Test("/Users/alan/Desktop/test.pst");

		SpringApplication.run(LibpstDemoApplication.class, args);
	}

    // This example reads a PST and prints the tree structure to the console
	public static void Test(String filename) {
		try {
			PSTFile pstFile = new PSTFile(filename);
			System.out.println(pstFile.getMessageStore().getDisplayName());
			processFolder(pstFile.getRootFolder());
		} catch (Exception err) {
			err.printStackTrace();
		}
	}

	static int depth = -1;
	public static void processFolder(PSTFolder folder)
			throws PSTException, java.io.IOException
	{
		depth++;
		// the root folder doesn't have a display name
		if (depth > 0) {
			printDepth();
			System.out.println(folder.getDisplayName());
		}

		// go through the folders...
		if (folder.hasSubfolders()) {
			Vector<PSTFolder> childFolders = folder.getSubFolders();
			for (PSTFolder childFolder : childFolders) {
				processFolder(childFolder);
			}
		}

		// and now the emails for this folder
		if (folder.getContentCount() > 0) {
			depth++;
			PSTMessage email = (PSTMessage)folder.getNextChild();
			while (email != null) {
				printDepth();
				System.out.println("Email: "+email.getSubject());
                System.out.println(email.getEmailAddress());
                System.out.println(email.getDisplayTo());
                System.out.println(email.getBody());
                try {
                    getAttachment(email);
                } catch (Exception e) {
                    //
                }
				email = (PSTMessage)folder.getNextChild();
			}
			depth--;
		}
		depth--;
	}

	public static void printDepth() {
		for (int x = 0; x < depth-1; x++) {
			System.out.print(" | ");
		}
		System.out.print(" |- ");
	}

    public static void getAttachment(PSTMessage email) throws Exception {
        int numberOfAttachments = email.getNumberOfAttachments();
        for (int x = 0; x < numberOfAttachments; x++) {
            PSTAttachment attach = email.getAttachment(x);
            InputStream attachmentStream = attach.getFileInputStream();
            // both long and short filenames can be used for attachments
            String filename = attach.getLongFilename();
            if (filename.isEmpty()) {
                filename = attach.getFilename();
            }
            FileOutputStream out = new FileOutputStream(filename);
            // 8176 is the block size used internally and should give the best performance
            int bufferSize = 8176;
            byte[] buffer = new byte[bufferSize];
            int count = attachmentStream.read(buffer);
            while (count == bufferSize) {
                out.write(buffer);
                count = attachmentStream.read(buffer);
            }
            byte[] endBuffer = new byte[count];
            System.arraycopy(buffer, 0, endBuffer, 0, count);
            out.write(endBuffer);
            out.close();
            attachmentStream.close();
        }
    }

}
